# Meteor stress test

## Prerequisites

- nodejs (6+) installed for `meteor-down`
- Python (2.7+) installed for `locust`

## *Meteor-down* to stress-test Meteor back-end methods

### Starting Meteor-down

Assuming you've a Meteor.js application running on `http://localhost:3000`, you can using meteor-down.

`url` (METEOR_URI) and `concurrency` (NB_USERS) can be changed in `load-test.js` file, depending the stress test, update accordingly like below:

``` JavaScript
const url = 'http://localhost:3000' // METEOR_URI
const concurrency = 1000 // NB_USERS
```

`cd meteor-down`

`yarn && yarn start`

Of course it'll be more efficient and realistic with a database filled with a replica of production data, or mock object data.

## Improvments

- Defining more specific testing scenarios, ideally covering all Meteor defined Methods
- ...


## *Locust* to stress-test an app simulating user interactions at scale

### Ease testing w/ authentication

In `/client/main.js` you can add something like this to have an user logged when app starts

``` JavaScript
Meteor.startup(() => {
  Session.set('showVisible', true)
  const test_env = true // process.env.METEOR_TEST (beware it'll be a string not a boolean)
  Promise.all([
    ...(test_env ? Meteor.loginWithPassword('bob', '123456') : null),
    ReactDOM.render(renderRoutes(), document.getElementById('root'))
  ])
})
```
You can have a secret configuration file with several user credentials and take randomly one in the list to have different users connected during the stress-test.

### Starting Locust

`cd locust`

1) Assuming pip is installed on testing machine, install dependencies

`make install`

2) Then to start locust

`make start`

3) Then open a browser, and go to `http:localhost:8089`

4) select the number of users you want to simulate and the rate/s

### Improvments

- Defining more specific testing scenarios, having fake users triggering events on pages, changing randomly to another page
- Automatizing locust testing in a dedicated environment, e.g:  everytime a new commit is done, create a Docker Image for the Meteor App, and a Docker image for the locust runtime 
- ...
