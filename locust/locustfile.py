from locust import HttpLocust, TaskSet

def index(l):
    l.client.get("/meeting")

class UserBehavior(TaskSet):
    tasks = {index: 1}

    def on_start(self):
       pass

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
