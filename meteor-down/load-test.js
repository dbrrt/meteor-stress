const url = 'http://localhost:3000' // METEOR_URI
const concurrency = 1000 // NB_USERS

meteorDown.init((Meteor) =>  {
  Promise.all([
    Meteor.call('users.listForMeeting', (error, result) => {
      console.log(result)
    })
  ])
  .then(() => Meteor.kill())
})

meteorDown.run({ concurrency, url })
